import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TheController {

	private TheView theView;
	private TheModel theModel;
	
	public TheController(TheView theView,TheModel theModel){
		
		this.theView = theView;
		this.theModel = theModel;
		this.theView.addCalculationListener(new CalculateListener());

	}
	
	class CalculateListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			int numberOne, numberTwo = 0;
			
			try{
				numberOne = theView.getNumberOne();
				numberTwo = theView.getNumberTwo();
				
				theModel.addTwoNumbers(numberOne, numberTwo);
				
				theView.setCalculationSolution(theModel.getCalculationResult());
			}
			catch(NumberFormatException ex){
				theView.displayErrorMessage("You need to enter 2 integers");
			}
		}
		
		
	}
	
}

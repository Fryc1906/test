
public class TheModel {
	
	private int calculationResult;
	
	public void addTwoNumbers(int numberOne, int numberTwo){
		
		calculationResult = numberOne + numberTwo;
		
	}
	
	public int getCalculationResult(){
		
		return calculationResult;
	
	}
}

import java.awt.event.ActionListener;

import javax.swing.*;

public class TheView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField numberOne = new JTextField(10);
	private JLabel additionLabel = new JLabel("+");
	private JTextField numberTwo = new JTextField(10);
	private JButton calculateButton = new JButton("Calculate");
	private JTextField calculationSolution = new JTextField(10);
	
	TheView(){
		
		JPanel calcPanel = new JPanel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600,200);
		
		calcPanel.add(numberOne);
		calcPanel.add(additionLabel);
		calcPanel.add(numberTwo);
		calcPanel.add(calculateButton);
		calcPanel.add(calculationSolution);
		
		this.add(calcPanel);
	
	}
	
	public int getNumberOne(){
		
		return Integer.parseInt(numberOne.getText());
		
	}
	
	public int getNumberTwo(){
		
		return Integer.parseInt(numberTwo.getText());
		
	}
	
	public int getCalculationSolution(){
		
		return Integer.parseInt(calculationSolution.getText());
		
	}

	public void setCalculationSolution(int solution){
		
		calculationSolution.setText(Integer.toString(solution));
	}
	
	public void addCalculationListener(ActionListener listenerCalc){
		
		calculateButton.addActionListener(listenerCalc);
	}
	
	public void displayErrorMessage(String errorMessage){
		
		JOptionPane.showMessageDialog(this,errorMessage);
	}
}

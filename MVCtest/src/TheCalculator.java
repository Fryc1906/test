
public class TheCalculator {

	public static void main(String[] args){
		
		TheView theView = new TheView();
		TheModel theModel = new TheModel();
		TheController theController = new TheController(theView,theModel);
		
		theView.setVisible(true);
	}
}
